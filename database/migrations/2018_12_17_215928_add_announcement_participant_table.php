<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnnouncementParticipantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcement_participant', function (Blueprint $table) {
            
            $table->increments('id');
            
            $table->integer('announcementId')->index()->unsigned();
            $table->foreign('announcementId')->references('announcementId')->on('announcements')->onDelete('cascade');
            
            $table->integer('participantId')->index()->unsigned();
            $table->foreign('participantId')->references('participantId')->on('participants')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcement_participant');
    }
}
