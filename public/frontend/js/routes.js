config = [
    "$locationProvider",
    "$stateProvider",
    '$urlRouterProvider',
    function($locationProvider, $stateProvider, $urlRouterProvider) {
        
        $stateProvider
        
        .state('app',{
            url: "/",
            templateUrl: "frontend/templates/main.component.html",
            controller: 'mainController',
            abstract: true
        })    
        
        .state('app.home',{
            url: "announcements",
            template: "<announcements-component></announcements-component>"
        })
        
        .state({
            name: 'app.announcement',
            url: "announcements/:id",
            template: "<announcement-setup-component></announcement-setup-component>"
        });
        
    
        $urlRouterProvider.otherwise("/");
        
        $urlRouterProvider.when("/", "/announcements");
        
    }
];
