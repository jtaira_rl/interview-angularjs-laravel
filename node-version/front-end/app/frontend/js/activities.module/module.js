

angular.module('activities.module', []);

angular.module('activities.module').service('announcementService', announcementService);

angular.module('activities.module').service('participantService', participantService);

angular.module('activities.module').component('announcementsComponent', announcementsComponent);

angular.module('activities.module').component('announcementCardComponent', announcementCardComponent);

angular.module('activities.module').component('announcementSetupComponent', announcementSetupComponent);

