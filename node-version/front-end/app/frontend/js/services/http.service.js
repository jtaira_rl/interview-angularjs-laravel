

httpService.$inject = ['$http', '$q'];


function httpService($http, $q) {
    
    
    
    /**
     * HTTP request function to the server.
     * @param  {string} method  "GET", "POST", "PUT", "DELETE"
     * @param  {string} url     ex "/v1/activities"
     * @param  {obj} params     Data to be send to the server
     * @return {promise}        request promise
     */
    this.sendRequest = function (method, url, params) {
      
        var deferred = $q.defer();
      
        var request;
        
        //TODO validate params passed to the method
      
        switch (method.toLowerCase()) {
            case 'get':
                
                request = $http.get(url, params);
    
                //TODO - validate that the params are being passed correctly - Query string params structure required
                
                break;
                
            case 'post':
            
                //todo validate that you are passing data to the post url
               
                request = $http.post(url, params);
                
                break;
            
            case 'put':
                
                //todo validate that you are passing data to the put url
                
                request = $http.put(url, params);
              
                break;
        
            case 'delete':
        
                request = $http.delete(url);
        
                break;
    
        }
    
        if (!request) {
            
            throw new Error("Undefined method " + "'" + method + "'");
         
        }
        
        request.then(function (response) {
           
            deferred.resolve(response.data);

        }, function (error) {
           
            deferred.reject(error);
        
            
        });

        return deferred.promise;
        
    };
    
}