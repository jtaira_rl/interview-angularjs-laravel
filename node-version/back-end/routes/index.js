module.exports = (server) => {
    
    require('./home.routes')(server);

    require('./api.routes')(server);

};
