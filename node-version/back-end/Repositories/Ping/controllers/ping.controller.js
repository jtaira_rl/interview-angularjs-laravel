const sendPing = (request, response, next) => {

    response.send({
        ping: 'ok',
        status: 200,
        date: new Date()
    });

    return next();
};


module.exports = {
    sendPing
};
