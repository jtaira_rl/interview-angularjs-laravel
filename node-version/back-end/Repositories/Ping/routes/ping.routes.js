const config = require('../../../config');
const PingController = require('../controllers/ping.controller');

module.exports = (server) => {

    const PATH = config.basePath('/ping');

    server.get({
        path: PATH,
        version: '1.0.0',
    }, [PingController.sendPing]);

};

//end
