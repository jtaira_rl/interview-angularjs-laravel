const restify = require('restify');
const config = require('./config');
const restifyPlugins = require('restify-plugins');
const jwt = require('restify-jwt-community');
const process = require('process');
const corsMiddleware = require('restify-cors-middleware');


// 'Access-Control-Allow-Origin'
const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: [
        'authorization',
        'content-type',
        'Access-Control-Allow-Origin',
    ],
    exposeHeaders: ['*'],
    credentials: false,
});

/**
 * Initialize Server
 */
const server = restify.createServer({
    name: config.name,
    version: config.version,
});

server.server.setTimeout(60000 * 20);

server.pre(cors.preflight);

server.use(cors.actual);

// Auth
const jwtConfig = {
    // issuer: 'https://activityright.dev/api/auth/authenticate',
    secret: config.JWT_SECRET,
};

// secure all routes. except /the following
server.use(jwt(jwtConfig).unless({
    path: [
        '/',
        '/node-api/ping'
    ],
}));


/**
 * Middleware
 */
server.use(restifyPlugins.multipartBodyParser({
    mapParams: true,
}));

server.use(restifyPlugins.jsonBodyParser({
    mapParams: true,
}));

server.use(restifyPlugins.acceptParser(server.acceptable));

server.use(restifyPlugins.queryParser({
    mapParams: true,
}));

server.use(restifyPlugins.fullResponse());

/**
 * Start Server, Connect to DB & Require Routes
 */
server.listen(config.port, () => {
    // SequelizeModule.initConnection();
    require('./routes')(server);
});

process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
});


module.exports = server;